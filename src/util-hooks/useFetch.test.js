import { renderHook } from "@testing-library/react-hooks";
import { useFetch } from "./useFetch";

describe("confirm that our customHook work", () => {
  test("Get the result of the call from api", async () => {
    const { result, waitForNextUpdate } = renderHook(() => useFetch());

    await waitForNextUpdate({ timeout: 1000 });

    const { transformedResponse } = result?.current?.data;
 
    const { data } = transformedResponse;

    expect(data).toStrictEqual({
      id: 1,
      name: "Chema",
      surname: "García",
      email: "chema@cloudframework.io",
      phone: "34646009988",
      age: 21,
    });
  });
});