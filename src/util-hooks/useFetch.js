import { useState, useEffect } from "react";
import { returnActualUrlId } from "../utils/returnActualUrlId";

export const useFetch = () => {
  const [data, setData] = useState(null);
  useEffect(() => {
    fetch(process.env.REACT_APP_USERS + returnActualUrlId())
      .then(async (getResponse) => {
        const transformedResponse = await getResponse.json();
        if (!transformedResponse.success) {
          setData(transformedResponse.message);
        }
        return transformedResponse;
      })
      .then((transformedResponse) => {
    
        setData({
          transformedResponse,
        });
      })
      .catch((error) => {
        setData({ errorMessage: error.toString() });
        console.error("There was an error!", error);
      });
  }, []);

  return {
    data,
  };
};
