import React from 'react';
import ReactDOM from 'react-dom';

import './index.scss';
import CloudFrameworksApp from './CloudFrameworksApp';
import { Provider } from 'react-redux';
import store from './redux/store'

ReactDOM.render(
<Provider store={store}>
   <CloudFrameworksApp />
</Provider>
, document.getElementById('root'));