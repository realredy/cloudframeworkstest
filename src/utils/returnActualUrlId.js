const actualUrl = new URLSearchParams(document.location.search);
export const returnActualUrlId = () => {
  const returnId = actualUrl.get("id") || "";
  return returnId;
};
