const SavingDataCloud = (dataFromFormInputs) => {
  const formData = new FormData(dataFromFormInputs.target);
  const formEntries = Object.fromEntries(formData);

  const saveResults = fetch(process.env.REACT_APP_SAVEDATA + formEntries.id, {
    method: "POST",
    body: JSON.stringify(formEntries),
    headers: {
      "X-WEB-KEY": "Development",
    },
  })
    .then(async (getResponse) => {
      const transformedResponse = await getResponse.json();
      return transformedResponse;
    })
    .then((transformedResponse) => {
      return transformedResponse;
    })
    .catch((error) => {
      console.error("There was an error!", error);
    });
  return saveResults;
};

export default SavingDataCloud;
