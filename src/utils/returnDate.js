/**
 *-padStart-Rellena la cadena actual con una cadena determinada 
 (posiblemente repetida) para que la cadena resultante 
 alcance una longitud determinada. El relleno se aplica
  desde el inicio (izquierda) de la cadena actual. 
  2 es el maximo que se busca alcanzar 
  0 es lo que inyectará para llenar la cadena
 */


  const getDateNowAndAddOneMont = (() => {
    const date = new Date();
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const day = String(date.getDate()).padStart(2, '0');
    const year = date.getFullYear();
     return `${year}-${month}-${day}`;
  })();
   export default getDateNowAndAddOneMont;