import ConfirmationSavedPannel from "./components/layout/ConfirmationSaveProcess";
import { useFetch } from "./util-hooks/useFetch";
import WaitModule from "./components/layout/WaitModule";
import { useDispatch, useSelector } from "react-redux";
import Layout from "./components/layout/Layout";
import Form from "./components/layout/Form";
import SavingDataCloud from "./utils/savingDataToCloud";
import controlScrollBlocking from "./utils/controlScrollBlocking";
import {
  errorDataSaving,
  finishDataSaving,
  getAllData,
  initData,
} from "./redux/userReducer/userSlice";

function CloudFrameworksApp() {
  const collection = useFetch();
  const { data } = collection;
  const userInformation = data?.transformedResponse;

  const status = useSelector((state) => state.user.sta);
  
  controlScrollBlocking(status);

  const dispatcher = useDispatch();

  const closePannel = () => dispatcher(finishDataSaving());

  const sendDataToFather = async (dataFromFormInputs) => {
    dispatcher(initData());
    const getResultsFromDataSaved = await SavingDataCloud(dataFromFormInputs);
    !getResultsFromDataSaved.success &&
      dispatcher(errorDataSaving(getResultsFromDataSaved.message));
    getResultsFromDataSaved.success &&
      dispatcher(getAllData(getResultsFromDataSaved.data));
  };

  return (
    <div data-test="app">
      {status && <ConfirmationSavedPannel closePannel={closePannel} />}
      <Layout>
        {userInformation ? (
          <Form dataFromCloud={userInformation} sendDataToFather={sendDataToFather} />
        ) : (
          <WaitModule />
        )}
      </Layout>
    </div>
  );
}

export default CloudFrameworksApp;
