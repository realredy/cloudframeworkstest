import { configureStore } from '@reduxjs/toolkit'; 

import userReducer from './userReducer/userSlice';

const Store = configureStore( {
    reducer:{
       user:  userReducer
    } });
export default Store; 