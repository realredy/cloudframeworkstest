import { combineReducers } from 'redux'; 
import userReducer from '../userReducer/userSlice'; 

const storagesReducers = combineReducers({  
    userReducer:  userReducer
}); 

export default storagesReducers;