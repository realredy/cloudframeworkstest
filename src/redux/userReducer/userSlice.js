import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  sta: false,
  data: false,
  savedData: false,
  saveDataError: false,
};

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    initData: (state) => {
      return {
        ...state,
        saveDataError: false,
        data: false,
        sta: true,
      };
    },
    getAllData: (state, action) => {
      return {
        ...state,
        saveDataError: false,
        data: action.payload,
      };
    },
    finishDataSaving: (state) => {
      return {
        ...state,
        sta: false,
      };
    },
    errorDataSaving: (state, action) => {
      return {
        ...state,
        saveDataError: action.payload,
      };
    },
  },
});

export const { initData, getAllData, finishDataSaving, errorDataSaving } =
  userSlice.actions;
export default userSlice.reducer;
