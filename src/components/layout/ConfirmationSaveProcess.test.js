import { render, screen, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";

import { Provider } from "react-redux";
import Store from "../../redux/store";
import ConfirmationSaveProcess from "./ConfirmationSaveProcess";
import {
  errorDataSaving,
  getAllData,
} from "../../redux/userReducer/userSlice";

//* to check status of the redux   console.info('💏', Store.getState()) *//

describe("Test behavior of module Confirmation", () => {
  const dataFake = {
    code: 201,
    data: {
      age: "35",
      email: "fran@cloudframework.io",
      id: 2,
      loan_amount: "11",
      loan_weeks: "1",
      name: "Fran",
      phone: "4488776673888",
      surname: "Herrera",
    },

    status: 201,
    success: true,
  };

  let rend;
  beforeEach(() => {
    rend = render(
      <Provider store={Store}>
        <ConfirmationSaveProcess />
      </Provider>
    );
  });

  it("Confirm Spiner module jump in while data is charging", async () => {
    expect(screen.getByTestId(/spinner/i)).toBeInTheDocument();
  });

  it("expect show error module", () => {
    const dataFakeModified = {
      ...dataFake,
      status: 400,
      success: false,
      message: "How this fake error module",
      data: false,
    };
    Store.dispatch(errorDataSaving(dataFakeModified.message));

    expect(screen.getByRole("ErrorPannel")).toBeInTheDocument();
  });

  it("Show info on table with the content", () => {
    const checkSomeDataInTable = [
      "fran@cloudframework.io",
      "4488776673888",
      "Herrera",
      "Fran",
    ];

    Store.dispatch(getAllData(dataFake.data));
    Store.dispatch(errorDataSaving(false));
    expect(screen.getByRole("ContenWithData")).toBeInTheDocument();
    expect(
      screen.getByText("Datos guardados satisfactoriamente..")
    ).toBeInTheDocument();
    checkSomeDataInTable.map((items) => {
      expect(screen.getByText(items)).toBeInTheDocument();
    });
  });
});
afterEach(() => {
  Store.dispatch(errorDataSaving(false));
  Store.dispatch(getAllData(false));
});