  import './errorModule.scss';

export default function ErrorMessage({error}) {
  return (
    <>
      <div className='ErrorMessage'>
        <b>
            {error}
        </b>
        <p>Please inser the id on the url like: ?id=1</p>
      </div>
    </>
  );
}