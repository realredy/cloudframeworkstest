import { shallow, mount } from "enzyme";
import ErrorMessage from './ErrorMessage'


describe('Error mensaje.',()=>{
  
 test("renders the navigation component", () => {
    
    const wrapper = shallow(<ErrorMessage error={'Error: '} />);
     
 expect(wrapper.find({className:'ErrorMessage'}).text()).toBe('Error: Please inser the id on the url like: ?id=1');
  });

});