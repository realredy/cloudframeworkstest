import './waitModule.scss';

export default function WaitModule() {
  return (
    <>
      <div className='WaitModule'>
        <b role="showMessageChargin" className='WaitModule-b'>
            Chargin Data, Wait please....
        </b>
      </div>
    </>
  );
}