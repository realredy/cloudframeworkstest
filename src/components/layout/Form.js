import getDateNow from "../../utils/returnDate";
import ErrorMessage from "./ErrorMessage";
import "./form.scss";

export default function Form({ children, dataFromCloud, sendDataToFather }) {
  
  const sendAlldataFromForm = (event) => {
    event.preventDefault();
    sendDataToFather(event);
  };

  if (!dataFromCloud.success) {
    return <ErrorMessage />;
  }
  return (  
    <>
      <div className="main_sides_right-form">
        <h2>Introdusca los datos en el formulario</h2>

        <form
          onSubmit={(event) => {
            sendAlldataFromForm(event);
          }}
          action="javascript:void(0);"
          className="main_sides_right-form_inner"
        >
          <div className="main_sides_right-form_inner_name">
            <span>Nombre </span>
            <input
              type="text"
              name="name"
              defaultValue={dataFromCloud.data.name}
              readOnly
            />
          </div>
          <div className="main_sides_right-form_inner_surname">
            <span>Apellidos </span>
            <input
              type="text"
              name="surname"
              defaultValue={dataFromCloud.data.surname}
              readOnly
            />
          </div>
          <div className="main_sides_right-form_inner_email">
            <span>Email </span>
            <input
              type="email"
              name="email"
              defaultValue={dataFromCloud.data.email}
              readOnly
            />
          </div>
          <div className="main_sides_right-form_inner_phone">
            <span>Teléfono* </span>
            <input
              type="text"
              name="phone"
              defaultValue={dataFromCloud.data.phone}
              required
            />
          </div>
          <div className="main_sides_right-form_inner_edad">
            <span>Edad* </span>
            <input
              type="number"
              name="age"
              defaultValue={dataFromCloud.data.age}
              required
            />
          </div>
          <div className="main_sides_right-form_inner_prestamo">
            <span>Prestamo (monto a solicitar) min: 11 - max: 1000* </span>
            <input
            role="RoleNumber"
             id="setNumber"
              type="number"
              name="loan_amount"
              min="11"
              max="1000"
              required
            />
          </div>
          <div className="main_sides_right-form_inner_inicio">
            <span>Fecha (cuando desea conseguir el prestamo)* </span>
            <input role="setDate" type="date" name="loan_date" min={getDateNow} required />
          </div>
          <div className="main_sides_right-form_inner_tiempo">
            <span>Tiempo a devover el prestamo (1-20 años)* </span>
            <input role="setTimeReturn" type="number" name="loan_weeks" min="1" max="20" required />
          </div>
          <div className="main_sides_right-form_inner_aceptar">
            <input role="Chec" type="checkbox" name="check" required />
            <span>
              Acepte los{" "}
              <a
                href="https://cloudframework.io/terminos-y-condiciones/"
                target="blank"
              >
                terminos y condiciones*
              </a>
            </span>
          </div>

          <div className="main_sides_right-form_inner_send">
            <input role="sendDataForm" type="submit" value="solicitar" />
          </div>
          <input name="id" type="hidden" defaultValue={dataFromCloud.data.id} />
        </form>
        {children}
      </div>
    </>
  );
}
