
import './layout.scss';

export default function Layout({children}) {
  return (
    <>
      <main data-test="LAyout" className="main">
      {/* <img className="conten_gellery-header-sortBy-img" src={process.env.PUBLIC_URL + '/sort.png'} alt='sort image' /> */}
      <div className="main_sides">
      <div className="main_sides_left">
        <h1 role="GenTitle" className="main_sides_left-h1">Get money so fast as never has imagined<span style={{color:'rgb(48, 184, 199)'}}>.</span></h1>
        </div>
        <div className="main_sides_right">
        {children}
        </div> 
        </div> 
        </main>
    </>
  );
}
