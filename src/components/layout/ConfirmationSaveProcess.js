import "./ConfirmationSaveProcess.scss";
import { useSelector } from "react-redux";

export default function ConfirmationSavedPannel({ closePannel }) {
  const status = useSelector((state) => state.user.sta);
  const datos = useSelector((state) => state.user.data);

  const error = useSelector((state) => state.user.saveDataError);

 

  const closePannelOfResults = () => {
    closePannel();
  };

  const savedResponseContainer = () => {
    if (error) {
      return (
        <div className="errorSignal" role="ErrorPannel">
          <div className="errorSignal_wrapper">
            <button role="CloseBTN"
              onClick={() => {
                closePannelOfResults();
              }}
              className="confirmationStatus_wrapper_closeBTN"
            >
              +
            </button>
            <div className="errorSignal_wrapper-message ErrorMessage">
              Se ha generado un error al intentar guardar los datos:  
               <pre role="showErrorMessage">{error}</pre>
            </div>
          </div>
        </div>
      );
    }

    if(!datos){
     return( <>
     <div data-testid="spinner" className="spinner"></div>
     </>) 
    } 

    if (datos) {
      return (
        <div role="ContenWithData" className="confirmationStatus">
          <div className="confirmationStatus_wrapper">
            <button
              role="CloseBTN"
              onClick={() => {
                closePannelOfResults();
              }}
              className="confirmationStatus_wrapper_closeBTN"
            >
              +
            </button>
            <h4 role="confirm">Datos guardados satisfactoriamente..</h4>
           
            <table cellPadding="0" cellSpacing="0">
              <thead>
                <tr>
                  <td>key</td>
                  <td>value</td>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>nombre</td>
                  <td>{datos.name}</td>
                </tr>
                <tr>
                  <td>apellido</td>
                  <td>{datos.surname}</td>
                </tr>
                <tr>
                  <td>email</td>
                  <td>{datos.email}</td>
                </tr>
                <tr>
                  <td>teléfono</td>
                  <td>{datos.phone}</td>
                </tr>
                <tr>
                  <td>edad</td>
                  <td>{datos.age}</td>
                </tr>
                <tr>
                  <td>monto requerido</td>
                  <td>{datos.loan_amount}</td>
                </tr>
                <tr>
                  <td>devolver en</td>
                  <td>{datos.loan_weeks} años</td>
                </tr>
              </tbody>
              <tfoot>
                <tr>
                  <td>key</td>
                  <td>value</td>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      );
    }
  };

  return (
    <>
      <section className="saveProcess" data-test="navigation-header">
        {savedResponseContainer()}
      </section>
    </>
  );
}
