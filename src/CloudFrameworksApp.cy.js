import React from "react";
import CloudFrameworksApp from "./CloudFrameworksApp";
import { Provider } from "react-redux";
import Store from "./redux/store";

describe("<CloudFrameworksApp />", () => {
  it("renders the software and show the workflow", () => {
    cy.viewport(550, 950)
    cy.mount(
      <Provider store={Store}>
        <CloudFrameworksApp />
      </Provider>
    );

    cy.get(".WaitModule-b").should(
      "have.text",
      "Chargin Data, Wait please...."
    );

    cy.get(".WaitModule-b").should(
      "have.text",
      "Chargin Data, Wait please...."
    );

    cy.get('[name="name"]', { timeout: 2000 }).should("have.value", "Chema");
    cy.get('[name="surname"]', { timeout: 500 }).should("have.value", "García");
    cy.get('[type="email"]', { timeout: 500 }).should(
      "have.value",
      "chema@cloudframework.io"
    );
    cy.get('[name="phone"]', { timeout: 500 }).should(
      "have.value",
      "34646009988"
    );

    cy.get("#setNumber").then(function ($input) {
      $input[0].setAttribute("value", "15");
    });
    cy.wait(1000)
    cy.get('[name="loan_date"]').then(function ($input) {
      $input[0].setAttribute("value", "2023-10-10");
    });
    cy.wait(1000)
    cy.get('[role="setTimeReturn"]').then(function ($input) {
      $input[0].setAttribute("value", "14");
    });
    cy.wait(1000)
    cy.get('[role="Chec"]').then(function ($input) {
      $input[0].setAttribute("checked", "true");
    });

    cy.get('[role="sendDataForm"]').click();
     
    cy.get(".spinner", { timeout: 500 }).should("be.visible");

    cy.get(".confirmationStatus_wrapper", { timeout: 3000 }).should(
      "be.visible"
    );

    cy.get('[role="CloseBTN"]', { timeout: 3000 }).should("be.visible");

    cy.get('[role="CloseBTN"]').click();
  });



  it("show error: loan_amount is not a integer between 11 and 1000", () => {
   
    cy.mount(
      <Provider store={Store}>
        <CloudFrameworksApp />
      </Provider>
    );


    cy.get("#setNumber").then(function ($input) {
      $input[0].setAttribute("min", "1");
    });
    cy.get("#setNumber").then(function ($input) {
      $input[0].setAttribute("value", "5");
    });

    cy.get('[name="loan_date"]').then(function ($input) {
      $input[0].setAttribute("value", "2023-10-10");
    });

    cy.get('[role="setTimeReturn"]').then(function ($input) {
      $input[0].setAttribute("value", "14");
    });
    cy.get('[role="Chec"]').then(function ($input) {
      $input[0].setAttribute("checked", "true");
    });

    cy.get('[role="sendDataForm"]').click();
    cy.wait(1000)
    cy.get(".errorSignal", { timeout: 3000 }).should(
      "be.visible"
    );
    cy.get('[role="showErrorMessage"]', { timeout: 3000 }).should( "have.text", "loan_amount is not a integer between 11 and 1000");
    cy.wait(1000)
    cy.get('[role="CloseBTN" ]').click();
    cy.wait(1000)
    cy.get("#setNumber").then(function ($input) {
      $input[0].setAttribute("value", "15");
    });
    cy.get('[name="loan_date"]').then(function ($input) {
      $input[0].setAttribute("value", "2023-01-01");
    });
    cy.wait(1000)
    cy.get('[role="sendDataForm"]').click();
    cy.get('[role="showErrorMessage"]', { timeout: 3000 }).should( "have.text", "Loan_date is not a future date");
    cy.wait(1000)
    cy.get('[role="CloseBTN" ]').click();
    cy.wait(1000)
    cy.get('[name="loan_date"]').then(function ($input) {
      $input[0].setAttribute("value", "2023-10-10");
    });
    cy.wait(1000)
    cy.get('[role="setTimeReturn"]').then(function ($input) {
      $input[0].setAttribute("max", "4000");
    });
    cy.get('[role="setTimeReturn"]').then(function ($input) {
      $input[0].setAttribute("value", "100");
    });
    cy.wait(1000)
    
  
    cy.get('[role="sendDataForm"]').click();
    // cy.wait(1000)
     cy.get('[role="showErrorMessage"]', { timeout: 3000 }).should( "have.text", "Loan_weeks is not a integer between 11 and 20");
    // <pre role="showErrorMessage">loan_amount is not a integer between 11 and 1000</pre>

  })
 
});
