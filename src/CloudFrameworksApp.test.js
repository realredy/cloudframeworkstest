/* eslint-disable testing-library/await-async-query */
/* eslint-disable testing-library/no-debugging-utils */
import { shallow, mount } from "enzyme";

import "@testing-library/jest-dom";

import { render, screen, fireEvent } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

import CloudFrameworksApp from "./CloudFrameworksApp";
import { Provider } from "react-redux";
import Store from "./redux/store";

import { useFetch } from "./util-hooks/useFetch";

import "jest-location-mock";

import SavingDataCloud from "./utils/savingDataToCloud";
import returnDate from "./utils/returnDate";

/**
 * Factory funcion to create a ShallowWrapper for the App component
 * @function setup
 * @returns {ShallowWrapper}
 */
// const setup = () => shallow(<Provider store={Store}><CloudFrameworksApp /></Provider>);
// const findByTestAttr = (wrapper, val) => wrapper.find(`[data-test]='${val}'`);

jest.mock("./util-hooks/useFetch", () => {
  return { useFetch: jest.fn() };
});
jest.mock("./utils/savingDataToCloud", () => {
  const datos = {
    code: 201,
    data: {
      age: "35",
      email: "fran@cloudframework.io",
      id: 2,
      loan_amount: "11",
      loan_weeks: "1",
      name: "Fran",
      phone: "4488776673888",
      surname: "Herrera",
    },

    status: 201,
    success: true,
  };
  return {
    __esModule: true, // ! no entiendo bien esta parte
    default: jest.fn(() => datos),
    SavingDataCloud: jest.fn(),
  };
});

describe("test all f...", () => {
 
  beforeEach(() => {
  
    useFetch.mockReturnValue({
      data: {
        transformedResponse: {
          data: {
            age: 21,
            email: "chema@cloudframework.io",
            id: 1,
            name: "Chema",
            phone: "34646009988",
            surname: "García",
          },
          success: true,
        },
      },
    });
  });

  test("render module <WaitModule /> if no data charged", () => {
    useFetch.mockReturnValue(false);
   
    const frontApp = mount(
      <Provider store={Store}>
        <CloudFrameworksApp />
      </Provider>
    );
   
    // console.log(frontApp.debug());
    expect(frontApp.find({ role: "showMessageChargin" }).text()).toBe(
      "Chargin Data, Wait please...."
    );
  });

  it("render <Layout /> to show ehe title: Get Money as never has imagined", () => {
    useFetch.mockReturnValue(false);
    const frontApp = mount(
      <Provider store={Store}>
        <CloudFrameworksApp />
      </Provider>
    );
    expect(frontApp.find({ role: "GenTitle" }).text()).toBe(
      "Get money so fast as never has imagined."
    );
  });

  it("return the module <ErrorMessage /> if no set a result", () => {
    // when set success to false the module error module need be render
    useFetch.mockReturnValue({
      data: { transformedResponse: { data: {}, success: false } },
    });
    const frontApp = mount(
      <Provider store={Store}>
        <CloudFrameworksApp />
      </Provider>
    );
    //  console.log(frontApp.debug());
    const textToFind = "Please inser the id on the url like: ?id=1";
    expect(frontApp.find("ErrorMessage").children().find("p").text()).toBe(
      textToFind
    );
  });

  it("Return the module <Form /> if have data to fill the form", () => {
    const frontApp = mount(
      <Provider store={Store}>
        <CloudFrameworksApp />
      </Provider>
    );
    expect(frontApp.find("Form").exists()).toBe(true);
  });

  it("Confirm the items in the form are filled by default", () => {
    const frontApp = mount(
      <Provider store={Store}>
        <CloudFrameworksApp />
      </Provider>
    );
    const inputsToConfirnIsNotEmpty = [
      "name",
      "surname",
      "email",
      "phone",
      "age",
    ];
    const suggestResults = [
      "Chema",
      "García",
      "chema@cloudframework.io",
      "34646009988",
      "21",
    ];
    inputsToConfirnIsNotEmpty.map((items, key) => {
      expect(
        frontApp
          .find({ name: items })
          .html()
          .toString()
          .includes(suggestResults[key])
      ).toBe(true);
    });
  });

  it("Confirm data is saved sucessfully", async () => {
    SavingDataCloud.mockReturnValue({
      code: "form-params",
      data: {
        age: "35",
        email: "fran@cloudframework.io",
        id: 2,
        loan_amount: "12",
        loan_date: "2023-10-10", //`${ returnDate() }`,
        loan_weeks: "11",
        name: "Ricardo",
        phone: "940161623",
        surname: "Perez",
      },

      status: 200,
      success: true,
    });
    const frontApp = render(
      <Provider store={Store}>
        <CloudFrameworksApp />
      </Provider>
    );

    const input = screen.getByRole("RoleNumber");
    userEvent.type(input, "32", 200);
    userEvent.type(screen.getByRole("setDate"), "2023-09-09", 200);
    userEvent.type(screen.getByRole("setTimeReturn"), "2", 200);
    fireEvent.click(screen.getByRole("Chec"), 200);

    fireEvent.click(screen.getByRole("sendDataForm"), 200);

    // ! para indicar que se espere para que genere un proceso
    const outputSucessfullMessage = "Datos guardados satisfactoriamente..";
    expect(
      (await screen.findByRole("confirm", undefined, { timeout: 500 }))
        .textContent
    ).toBe(outputSucessfullMessage);
  });

  it("Show error Message if date is not a future date", async () => {
    SavingDataCloud.mockReturnValue({
      code: "form-params",
      data: {
        age: "35",
        email: "fran@cloudframework.io",
        id: 2,
        loan_amount: "12",
        loan_date: "2023-10-10", //`${ returnDate() }`,
        loan_weeks: "11",
        name: "Ricardo",
        phone: "940161623",
        surname: "Perez",
      },

      status: 400,
      success: false,
      message: "Loan_date is not a future date",
    });
    const frontApp = render(
      <Provider store={Store}>
        <CloudFrameworksApp />
      </Provider>
    );

    const input = screen.getByRole("RoleNumber");
    userEvent.type(input, "12", 200);
    userEvent.type(screen.getByRole("setDate"), "2023-01-01", 200);
    userEvent.type(screen.getByRole("setTimeReturn"), "2", 200);
    fireEvent.click(screen.getByRole("Chec"));

    fireEvent.click(screen.getByRole("sendDataForm"));

    const outputSucessfullMessage = "Loan_date is not a future date";
    expect(
      (await screen.findByRole("showErrorMessage", undefined, { timeout: 500 }))
        .textContent
    ).toBe(outputSucessfullMessage);
  });

  it("Show error Message if loan_amount is not correct", async () => {
    SavingDataCloud.mockReturnValue({
      code: "form-params",
      data: {
        age: "35",
        email: "fran@cloudframework.io",
        id: 2,
        loan_amount: "12",
        loan_date: "2023-10-10", //`${ returnDate() }`,
        loan_weeks: "11",
        name: "Ricardo",
        phone: "940161623",
        surname: "Perez",
      },

      status: 400,
      success: false,
      message: "loan_amount is not a integer between 11 and 1000",
    });
    const frontApp = render(
      <Provider store={Store}>
        <CloudFrameworksApp />
      </Provider>
    );

    const input = screen.getByRole("RoleNumber");
    userEvent.type(input, "32000", 200);
    userEvent.type(screen.getByRole("setDate"), "2023-11-11", 200);
    userEvent.type(screen.getByRole("setTimeReturn"), "2", 200);
    fireEvent.click(screen.getByRole("Chec"));

    fireEvent.click(screen.getByRole("sendDataForm"));

    const outputSucessfullMessage =
      "loan_amount is not a integer between 11 and 1000";
    // frontApp.debug()
    expect(
      (await screen.findByRole("showErrorMessage", undefined, { timeout: 500 }))
        .textContent
    ).toBe(outputSucessfullMessage);
  });

  it("Show error Message if loan_weeks is not correct", async () => {
    SavingDataCloud.mockReturnValue({
      code: "form-params",
      data: {
        age: "35",
        email: "fran@cloudframework.io",
        id: 2,
        loan_amount: "12",
        loan_date: "2023-10-10", //`${ returnDate() }`,
        loan_weeks: "11",
        name: "Ricardo",
        phone: "940161623",
        surname: "Perez",
      },

      status: 400,
      success: false,
      message: "Loan_weeks is not a integer between 11 and 20",
    });

    const frontApp = render(
      <Provider store={Store}>
        <CloudFrameworksApp />
      </Provider>
    );

    const input = screen.getByRole("RoleNumber");
    userEvent.type(input, "20", 200);
    userEvent.type(screen.getByRole("setDate"), "2023-11-11", 200);
    userEvent.type(screen.getByRole("setTimeReturn"), "288", 200);
    fireEvent.click(screen.getByRole("Chec"), 200);

    fireEvent.click(screen.getByRole("sendDataForm"), 200);

    const outputSucessfullMessage =
      "Loan_weeks is not a integer between 11 and 20";
    expect(
      (
        await screen.findByRole("showErrorMessage", undefined, {
          timeout: 4500,
        })
      ).textContent
    ).toBe(outputSucessfullMessage);
  });

  it("Hide error pannel on click event", async () => {
    SavingDataCloud.mockReturnValue({
      code: "form-params",
      data: {
        age: "35",
        email: "fran@cloudframework.io",
        id: 2,
        loan_amount: "12",
        loan_date: "2023-10-10", //`${ returnDate() }`,
        loan_weeks: "11",
        name: "Ricardo",
        phone: "940161623",
        surname: "Perez",
      },

      status: 200,
      success: false,
      message: "error message is now on the screen",
    });
    const frontApp = render(
      <Provider store={Store}>
        <CloudFrameworksApp />
      </Provider>
    );

    const input = screen.getByRole("RoleNumber");
    userEvent.type(input, "112", 200);
    userEvent.type(screen.getByRole("setDate"), "2023-09-09", 200);
    userEvent.type(screen.getByRole("setTimeReturn"), "244", 200);
    fireEvent.click(screen.getByRole("Chec"), 200);

    fireEvent.click(screen.getByRole("sendDataForm"), 200);

    expect(
      await screen.findByRole("ErrorPannel", undefined, { timeout: 500 })
    ).toBeInTheDocument();

    fireEvent.click(screen.getByRole("CloseBTN"), 200);

    expect(
      await screen.queryByRole("ErrorPannel", undefined, { timeout: 500 })
    ).toBeNull();
  });

  // it('Show error Message iif loan_amount is not correct',async () => {

  // jest.mock("./utils/savingDataToCloud", () => {
  //   const datos = { code: 201,
  //     data: {
  //       age: "35",
  //       email: "fran@cloudframework.io",
  //       id: 2,
  //       loan_amount: "11",
  //       loan_weeks: "1",
  //       name: "Fran",
  //       phone: "4488776673888",
  //       surname: "Herrera",
  //     },

  //     status: 201,
  //     success: true,}
  //   return {
  //     __esModule: true, // ! no entiendo bien esta parte
  //     default: jest.fn(() => datos),
  //      SavingDataCloud: jest.fn()
  //     };
  // });
  //   SavingDataCloud.mockReturnValue({
  //      code: 'form-params',
  //       data: {
  //         age: "35",
  //         email: "fran@cloudframework.io",
  //         id: 2,
  //         loan_amount: "20000",
  //         loan_date: '2023-06-10',//`${ returnDate() }`,
  //         loan_weeks: "1",
  //         name: "Ricardo",
  //         phone: "4488776673888",
  //         surname: "Perez",
  //       },

  //       status: 400,
  //       success: false,
  //       message: "loan_amount is not a integer between 11 and 1000"
  //   });

  //   const frontApp = render(
  //     <Provider store={Store}>
  //       <CloudFrameworksApp />
  //     </Provider>
  //   );

  //   fireEvent.click( screen.getByRole('sendDataForm') )

  //   const outputSucessfullMessage = 'loan_amount is not a integer between 11 and 1000';
  //  expect( (await screen.findByRole('showErrorMessage', undefined,{timeout: 2000})).textContent).toBe(outputSucessfullMessage)

  //   frontApp.debug()
  //     // Store.dispatch(todoAdded('Buy milk'))

  //   //   const { getByText } = renderWithProviders(<TodoList />, { store })

  // })
});

afterEach(() => {
  jest.clearAllMocks();
});
