import { Provider } from "react-redux"
import CloudFrameworksApp from "../CloudFrameworksApp"
import Store from "../redux/store"

 const WrapperModuleForTest = ({children}) =>{
      <Provider store={Store}>
    {children}
      </Provider>
         
}

export default WrapperModuleForTest